<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AlbumRepository")
 */
/**
 * @ORM\Entity
 * @Vich\Uploadable
 */
class Album
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Titre;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $DateDeSortie;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Artiste", inversedBy="Albums")
     */
    private $Artiste;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    /**
     * @Vich\UploadableField(mapping="Photo", fileNameProperty="Photo")
     */
    private $Photo;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Morceau", mappedBy="Album")
     */
    private $morceaux;

    public function __construct()
    {
        $this->Artiste = new ArrayCollection();
        $this->morceaux = new ArrayCollection();
    }
    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->getTitre();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->Titre;
    }

    public function setTitre(?string $Titre): self
    {
        $this->Titre = $Titre;

        return $this;
    }

    public function getDateDeSortie(): ?\DateTimeInterface
    {
        return $this->DateDeSortie;
    }

    public function setDateDeSortie(?\DateTimeInterface $DateDeSortie): self
    {
        $this->DateDeSortie = $DateDeSortie;

        return $this;
    }

    /**
     * @return Collection|Artiste[]
     */
    public function getArtiste(): Collection
    {
        return $this->Artiste;
    }

    public function addArtiste(Artiste $artiste): self
    {
        if (!$this->Artiste->contains($artiste)) {
            $this->Artiste[] = $artiste;
        }

        return $this;
    }

    public function removeArtiste(Artiste $artiste): self
    {
        if ($this->Artiste->contains($artiste)) {
            $this->Artiste->removeElement($artiste);
        }

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->Photo;
    }

    public function setPhoto(?string $Photo): self
    {
        $this->Photo = $Photo;

        return $this;
    }

    /**
     * @return Collection|Morceau[]
     */
    public function getMorceaux(): Collection
    {
        return $this->morceaux;
    }

    public function addMorceaux(Morceau $morceaux): self
    {
        if (!$this->morceaux->contains($morceaux)) {
            $this->morceaux[] = $morceaux;
            $morceaux->addAlbum($this);
        }

        return $this;
    }

    public function removeMorceaux(Morceau $morceaux): self
    {
        if ($this->morceaux->contains($morceaux)) {
            $this->morceaux->removeElement($morceaux);
            $morceaux->removeAlbum($this);
        }

        return $this;
    }
}
