<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MorceauRepository")
 */
/**
 * @ORM\Entity
 * @Vich\Uploadable
 */
class Morceau
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Titre;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $DateDeSortie;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $FichierMp3;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Genre;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    /**
     * @Vich\UploadableField(mapping="Photo", fileNameProperty="Photo")
     */
    private $Photo;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $Duree;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Artiste", inversedBy="morceaux")
     */
    private $Artiste;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Album", inversedBy="morceaux")
     */
    private $Album;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Instrument", inversedBy="morceaux")
     */
    private $Instruments;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Playlist", inversedBy="Morceau")
     */
    private $playlist;

    public function __construct()
    {
        $this->Artiste = new ArrayCollection();
        $this->Album = new ArrayCollection();
        $this->Instruments = new ArrayCollection();
    }
    public function __toString()
    {
        // TODO: Implement __toString() method.
       return $this->getTitre();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->Titre;
    }

    public function setTitre(?string $Titre): self
    {
        $this->Titre = $Titre;

        return $this;
    }

    public function getDateDeSortie(): ?\DateTimeInterface
    {
        return $this->DateDeSortie;
    }

    public function setDateDeSortie(?\DateTimeInterface $DateDeSortie): self
    {
        $this->DateDeSortie = $DateDeSortie;

        return $this;
    }

    public function getFichierMp3(): ?string
    {
        return $this->FichierMp3;
    }

    public function setFichierMp3(?string $FichierMp3): self
    {
        $this->FichierMp3 = $FichierMp3;

        return $this;
    }

    public function getGenre(): ?string
    {
        return $this->Genre;
    }

    public function setGenre(?string $Genre): self
    {
        $this->Genre = $Genre;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->Photo;
    }

    public function setPhoto(?string $Photo): self
    {
        $this->Photo = $Photo;

        return $this;
    }

    public function getDuree(): ?int
    {
        return $this->Duree;
    }

    public function setDuree(?int $Duree): self
    {
        $this->Durée = $Duree;

        return $this;
    }

    /**
     * @return Collection|Artiste[]
     */
    public function getArtiste(): Collection
    {
        return $this->Artiste;
    }

    public function addArtiste(Artiste $artiste): self
    {
        if (!$this->Artiste->contains($artiste)) {
            $this->Artiste[] = $artiste;
        }

        return $this;
    }

    public function removeArtiste(Artiste $artiste): self
    {
        if ($this->Artiste->contains($artiste)) {
            $this->Artiste->removeElement($artiste);
        }

        return $this;
    }

    /**
     * @return Collection|Album[]
     */
    public function getAlbum(): Collection
    {
        return $this->Album;
    }

    public function addAlbum(Album $album): self
    {
        if (!$this->Album->contains($album)) {
            $this->Album[] = $album;
        }

        return $this;
    }

    public function removeAlbum(Album $album): self
    {
        if ($this->Album->contains($album)) {
            $this->Album->removeElement($album);
        }

        return $this;
    }

    /**
     * @return Collection|Instrument[]
     */
    public function getInstruments(): Collection
    {
        return $this->Instruments;
    }

    public function addInstrument(Instrument $instrument): self
    {
        if (!$this->Instruments->contains($instrument)) {
            $this->Instruments[] = $instrument;
        }

        return $this;
    }

    public function removeInstrument(Instrument $instrument): self
    {
        if ($this->Instruments->contains($instrument)) {
            $this->Instruments->removeElement($instrument);
        }

        return $this;
    }

    public function getPlaylist(): ?Playlist
    {
        return $this->playlist;
    }

    public function setPlaylist(?Playlist $playlist): self
    {
        $this->playlist = $playlist;

        return $this;
    }
}
