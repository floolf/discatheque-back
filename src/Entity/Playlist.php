<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlaylistRepository")
 */
/**
 * @ORM\Entity
 * @Vich\Uploadable
 */
class Playlist
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Titre;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    /**
     * @Vich\UploadableField(mapping="Photo", fileNameProperty="Photo")
     */
    private $Photo;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Morceau", mappedBy="playlist")
     */
    private $Morceau;

    public function __construct()
    {
        $this->Morceau = new ArrayCollection();
    }
    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->getTitre();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->Titre;
    }

    public function setTitre(?string $Titre): self
    {
        $this->Titre = $Titre;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->Photo;
    }

    public function setPhoto(?string $Photo): self
    {
        $this->Photo = $Photo;

        return $this;
    }

    /**
     * @return Collection|Morceau[]
     */
    public function getMorceau(): Collection
    {
        return $this->Morceau;
    }

    public function addMorceau(Morceau $morceau): self
    {
        if (!$this->Morceau->contains($morceau)) {
            $this->Morceau[] = $morceau;
            $morceau->setPlaylist($this);
        }

        return $this;
    }

    public function removeMorceau(Morceau $morceau): self
    {
        if ($this->Morceau->contains($morceau)) {
            $this->Morceau->removeElement($morceau);
            // set the owning side to null (unless already changed)
            if ($morceau->getPlaylist() === $this) {
                $morceau->setPlaylist(null);
            }
        }

        return $this;
    }
}
