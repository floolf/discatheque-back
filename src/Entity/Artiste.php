<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArtisteRepository")
 */
/**
 * @ORM\Entity
 * @Vich\Uploadable
 */
class Artiste
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Nom;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $Biographie;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    /**
     * @Vich\UploadableField(mapping="Photo", fileNameProperty="Photo")
     */

    private $Photo;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Album", mappedBy="Artiste")
     */
    private $Albums;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Morceau", mappedBy="Artiste")
     */
    private $morceaux;
//    /**
//     * @Vich\UploadableField(mapping="file_photo", fileNameProperty="Photo")
//     */
//    private $photoFile;



    public function __construct()
    {
        $this->Albums = new ArrayCollection();
        $this->morceaux = new ArrayCollection();
    }
    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->getNom();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->Nom;
    }

    public function setNom(?string $Nom): self
    {
        $this->Nom = $Nom;

        return $this;
    }

    public function getBiographie(): ?string
    {
        return $this->Biographie;
    }

    public function setBiographie(?string $Biographie): self
    {
        $this->Biographie = $Biographie;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->Photo;
    }

    public function setPhoto(?string $Photo): self
    {
        $this->Photo = $Photo;

        return $this;
    }

    /**
     * @return Collection|Album[]
     */
    public function getAlbums(): Collection
    {
        return $this->Albums;
    }

    public function addAlbum(Album $album): self
    {
        if (!$this->Albums->contains($album)) {
            $this->Albums[] = $album;
            $album->addArtiste($this);
        }

        return $this;
    }

    public function removeAlbum(Album $album): self
    {
        if ($this->Albums->contains($album)) {
            $this->Albums->removeElement($album);
            $album->removeArtiste($this);
        }

        return $this;
    }

    /**
     * @return Collection|Morceau[]
     */
    public function getMorceaux(): Collection
    {
        return $this->morceaux;
    }

    public function addMorceaux(Morceau $morceaux): self
    {
        if (!$this->morceaux->contains($morceaux)) {
            $this->morceaux[] = $morceaux;
            $morceaux->addArtiste($this);
        }

        return $this;
    }

    public function removeMorceaux(Morceau $morceaux): self
    {
        if ($this->morceaux->contains($morceaux)) {
            $this->morceaux->removeElement($morceaux);
            $morceaux->removeArtiste($this);
        }

        return $this;
    }
}
