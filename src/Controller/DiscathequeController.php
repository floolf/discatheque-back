<?php

namespace App\Controller;

use App\Repository\AlbumRepository;
use App\Repository\ArtisteRepository;
use App\Repository\InstrumentRepository;
use App\Repository\MorceauRepository;
use App\Repository\PlaylistRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use phpDocumentor\Reflection\Types\This;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Artiste;
use App\Entity\Album;
use App\Entity\Instrument;
use App\Entity\Morceau;
use App\Entity\User;
use phpDocumentor\Reflection\DocBlock\Serializer;

class DiscathequeController extends AbstractController
{
    private $artisteRepository;
    private $albumRepository;
    private $instrumentRepository;
    private $morceauRepository;
    private $playlistRepository;
    private $userRepository;


    function __construct(
        ArtisteRepository $artisteRepository,
        AlbumRepository $albumRepository,
        InstrumentRepository $instrumentRepository,
        MorceauRepository $morceauRepository,
        PlaylistRepository $playlistRepository,
        UserRepository $userRepository
    )
    {
        $this->artisteRepository = $artisteRepository;
        $this->albumRepository = $albumRepository;
        $this->instrumentRepository = $instrumentRepository;
        $this->morceauRepository = $morceauRepository;
        $this->playlistRepository = $playlistRepository;
        $this->userRepository = $userRepository;

    }

    /**
     * @Route("/API/artistes", name="artistes")
     */
    public function Artistes()
    {


        $arrayArtistes = $this->artisteRepository->findAll();
        $this->getDoctrine();

        $arrayArtistes = $this->get('serializer')->serialize($arrayArtistes, 'json');

        return new JsonResponse(
            $arrayArtistes, 200, [], true
        );

    }

    /**
     * @Route("/API/albums", name="albums")
     */

    public function Albums()
    {
        $arrayAlbum = $this->albumRepository->findAll();
        $this->getDoctrine();

        $arrayAlbum = $this->get('serializer')->serialize($arrayAlbum, 'json');
//        dump($arrayAlbum);exit;
        return new JsonResponse(

            $arrayAlbum, 200, [], true
        );

    }

    /**
     * @Route("/API/instruments", name="instruments")
     */
    public function Instruments()
    {
        $arrayInstrument = $this->instrumentRepository->findAll();
        $this->getDoctrine();
        $arrayInstrument = $this->get('serializer')->serialize($arrayInstrument, 'json');
        return new JsonResponse(
            $arrayInstrument, 200, [], true
        );

    }

    /**
     * @Route("/API/morceaux", name="morceaux")
     */
    public function Morceaux()
    {
        $arrayMorceau = $this->morceauRepository->findAll();
        $this->getDoctrine();
        $arrayMorceau = $this->get('serializer')->serialize($arrayMorceau, 'json');
        return new JsonResponse(
            $arrayMorceau, 200, [], true
        );
    }

    /**
     * @Route("/API/playlists", name="playlists")
     */
    public function Playlists()
    {
        $arrayPlaylist = $this->playlistRepository->findAll();
        $this->getDoctrine();
        $arrayPlaylist = $this->get('serializer')->serialize($arrayPlaylist, 'json');
        return new JsonResponse(
            $arrayPlaylist, 200, [], true
        );

    }

    /**
     * @Route("/API/users", name="users")
     */
    public function Users()
    {
        $arrayUser = $this->userRepository->findAll();
        $this->getDoctrine();
        $arrayUser = $this->get('serializer')->serialize($arrayUser, 'json');
        return new JsonResponse(
            $arrayUser, 200, [], true
        );
    }





}
