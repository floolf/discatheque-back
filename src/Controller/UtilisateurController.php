<?php

namespace App\Controller;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class UtilisateurController extends AbstractController
{

    /**
     * @Route("/API/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils , Request $request): Response
    {
//         if ($this->getUser()) {
//             return $this->redirectToRoute('target_path');
//         }
           $user = json_decode($request->getContent(), true);
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
        return new JsonResponse(
            $user, 200, [], true);

    }

    /**
     * @Route("/API/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \Exception('This method can be blank - it will be intercepted by the logout key on your firewall');
    }
}
