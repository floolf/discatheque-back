<?php

namespace App\Repository;

use App\Entity\Pomme;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Pomme|null find($id, $lockMode = null, $lockVersion = null)
 * @method Pomme|null findOneBy(array $criteria, array $orderBy = null)
 * @method Pomme[]    findAll()
 * @method Pomme[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PommeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Pomme::class);
    }

    // /**
    //  * @return Pomme[] Returns an array of Pomme objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Pomme
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
