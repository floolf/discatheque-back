<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191209151726 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE album (id INT AUTO_INCREMENT NOT NULL, titre VARCHAR(255) DEFAULT NULL, date_de_sortie DATE DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE album_artiste (album_id INT NOT NULL, artiste_id INT NOT NULL, INDEX IDX_C9D0685D1137ABCF (album_id), INDEX IDX_C9D0685D21D25844 (artiste_id), PRIMARY KEY(album_id, artiste_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, pseudo VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE playlist (id INT AUTO_INCREMENT NOT NULL, titre VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE instrument (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) DEFAULT NULL, categorie VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE artiste (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) DEFAULT NULL, biographie VARCHAR(500) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE morceau (id INT AUTO_INCREMENT NOT NULL, playlist_id INT DEFAULT NULL, titre VARCHAR(255) DEFAULT NULL, date_de_sortie DATE DEFAULT NULL, fichier_mp3 LONGTEXT DEFAULT NULL, genre VARCHAR(255) DEFAULT NULL, duree INT DEFAULT NULL, INDEX IDX_36BB72086BBD148 (playlist_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE morceau_artiste (morceau_id INT NOT NULL, artiste_id INT NOT NULL, INDEX IDX_1E31A01529E8E5CE (morceau_id), INDEX IDX_1E31A01521D25844 (artiste_id), PRIMARY KEY(morceau_id, artiste_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE morceau_album (morceau_id INT NOT NULL, album_id INT NOT NULL, INDEX IDX_3674E3A429E8E5CE (morceau_id), INDEX IDX_3674E3A41137ABCF (album_id), PRIMARY KEY(morceau_id, album_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE morceau_instrument (morceau_id INT NOT NULL, instrument_id INT NOT NULL, INDEX IDX_9E1829FF29E8E5CE (morceau_id), INDEX IDX_9E1829FFCF11D9C (instrument_id), PRIMARY KEY(morceau_id, instrument_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE album_artiste ADD CONSTRAINT FK_C9D0685D1137ABCF FOREIGN KEY (album_id) REFERENCES album (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE album_artiste ADD CONSTRAINT FK_C9D0685D21D25844 FOREIGN KEY (artiste_id) REFERENCES artiste (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE morceau ADD CONSTRAINT FK_36BB72086BBD148 FOREIGN KEY (playlist_id) REFERENCES playlist (id)');
        $this->addSql('ALTER TABLE morceau_artiste ADD CONSTRAINT FK_1E31A01529E8E5CE FOREIGN KEY (morceau_id) REFERENCES morceau (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE morceau_artiste ADD CONSTRAINT FK_1E31A01521D25844 FOREIGN KEY (artiste_id) REFERENCES artiste (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE morceau_album ADD CONSTRAINT FK_3674E3A429E8E5CE FOREIGN KEY (morceau_id) REFERENCES morceau (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE morceau_album ADD CONSTRAINT FK_3674E3A41137ABCF FOREIGN KEY (album_id) REFERENCES album (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE morceau_instrument ADD CONSTRAINT FK_9E1829FF29E8E5CE FOREIGN KEY (morceau_id) REFERENCES morceau (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE morceau_instrument ADD CONSTRAINT FK_9E1829FFCF11D9C FOREIGN KEY (instrument_id) REFERENCES instrument (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE album_artiste DROP FOREIGN KEY FK_C9D0685D1137ABCF');
        $this->addSql('ALTER TABLE morceau_album DROP FOREIGN KEY FK_3674E3A41137ABCF');
        $this->addSql('ALTER TABLE morceau DROP FOREIGN KEY FK_36BB72086BBD148');
        $this->addSql('ALTER TABLE morceau_instrument DROP FOREIGN KEY FK_9E1829FFCF11D9C');
        $this->addSql('ALTER TABLE album_artiste DROP FOREIGN KEY FK_C9D0685D21D25844');
        $this->addSql('ALTER TABLE morceau_artiste DROP FOREIGN KEY FK_1E31A01521D25844');
        $this->addSql('ALTER TABLE morceau_artiste DROP FOREIGN KEY FK_1E31A01529E8E5CE');
        $this->addSql('ALTER TABLE morceau_album DROP FOREIGN KEY FK_3674E3A429E8E5CE');
        $this->addSql('ALTER TABLE morceau_instrument DROP FOREIGN KEY FK_9E1829FF29E8E5CE');
        $this->addSql('DROP TABLE album');
        $this->addSql('DROP TABLE album_artiste');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE playlist');
        $this->addSql('DROP TABLE instrument');
        $this->addSql('DROP TABLE artiste');
        $this->addSql('DROP TABLE morceau');
        $this->addSql('DROP TABLE morceau_artiste');
        $this->addSql('DROP TABLE morceau_album');
        $this->addSql('DROP TABLE morceau_instrument');
    }
}
